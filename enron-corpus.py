#!/usr/bin/env python

import sys
import os
import argparse
import email
import json
import io
import tarfile
import psycopg2
from psycopg2.extras import Json


def get_email(f):
    with tarfile.open(f) as tar:
        for tarinfo in tar:
            tar.members = []
            if tarinfo.isfile():
                data = tar.extractfile(tarinfo)
                data = io.TextIOWrapper(io.BytesIO(data.read()), 
                          encoding='cp1252')
                msg = email.message_from_file(data)
                ret = {k: v for k,v in msg._headers}
                ret[u'payload'] = msg._payload
                yield ret


def to_pgsql(gen, limit=None):
    DSN = 'host=localhost dbname=enron'
    with psycopg2.connect(DSN) as conn:
        curs = conn.cursor()
        curs.execute("CREATE TABLE enron (id SERIAL PRIMARY KEY, email json);")
        conn.commit()
        for idx, msg in enumerate(gen):
            if (limit is not None) and (idx >= limit):
                return 0
            else:
                curs.execute("INSERT INTO enron (email) VALUES (%s);", 
                             [Json(msg)])
                conn.commit()


def to_file(gen, limit=None):
    with open('enron.json', 'wt') as f:
        for idx, msg in enumerate(gen):
            if (limit is not None) and (idx >= limit):
                return 0
            else:
                f.write(json.dumps(msg))
                f.write('\n')


def to_stdout(gen, limit=None):
    for idx, msg in enumerate(gen):
        if (limit is not None) and (idx >= limit):
            return 0
        else:
            print json.dumps(msg)


def main():
    parser = argparse.ArgumentParser(description='Jsonify the Enron corpus')
    parser.add_argument('-l', '--limit', type=int, dest='limit', default=None,
                        help='number of records to process')
    parser.add_argument('-o', '--output', dest='output', default='file',
                        choices={'stdout', 'file', 'pgsql'}, 
                        help='output target')
    parser.add_argument(dest='filename', 
                        help='enron tarfile, e.g. enron_mail_20110402.tgz')

    args = parser.parse_args()

    if not os.path.exists(args.filename):
        sys.exit('{} is not a valid file'.format(args.filename))

    gen = get_email(args.filename)

    dispatch = {'stdout': to_stdout, 'file': to_file, 'pgsql': to_pgsql}
    dispatch[args.output](gen, args.limit)


if __name__ == '__main__':
    sys.exit(main())
